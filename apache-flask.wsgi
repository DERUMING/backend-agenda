#! /usr/bin/python

activate_this = '/home/debian/backend-agenda/env/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))
import sys
sys.path.insert(0, '/home/debian/backend-agenda')
from run import create_app
application = create_app("config")