from app import create_app
app = create_app('config')

@app.route("/hola", methods=['GET'])
def hola_mundo():
    return "<p>Hola mundo..!</p>"

@app.route("/usuario", methods=['POST'])
def usuario():
    return "<p>Hola desde servidor configurado con el metodo POST..!</p>"

if __name__ == '__main__':
    app.run()