# Instalación de Python

Ejecuta los siguientes comandos para instalar python3 en **(Debian 11)**

```
sudo apt update
sudo apt install python3-pip python3-dev python3-setuptools
```

Ejetuta los siguientes comandos para verificar la correcta instalación de los paquetes

```
python3 --version
pip --version
```

# Instalación de Virtualenv

Ejecuta el siguiente comando para instalar virtualenv

```
pip install virtualenv
```

Ejecuta el siguient comando para verificar la instalación del paquete

```
python3 -m virtualenv --version
```

# Instalación de Git

Ejecuta los siguientes comandos para instalar git en **(Debian 11)**

```
sudo apt update
sudo apt install git
```

# Despliegue del proyecto usando Nginx y Gunicorn

Descarga el proyecto desde el repositorio, ejecuta el siguiente comando dentro de un directorio de tú elección.

```
git clone https://gitlab.com/DERUMING/backend-agenda.git
```

Ejecuta el siguiente comando dentro del directorio del proyecto, para crear un entorno virutal

```
python3 -m virtualenv env
```

Ejecuta el siguiente comando dentro del directorio del proyecto, para activar el entorno vitual

```
source env/bin/activate
```

Ejecuta el siguiente comando despues de haber activado el entorno virtual, para instalar las librerias del proyecto

```
pip install -r requirements.txt
```

## Servidor de base de datos

Crea un usuario y una base de datos en tu servidor de base de datos, ejecuta los siguientes comandos dentro de la consola SQL Shell (psql) de PostgreSQL de tu servidor.

```
postgres=# create user python310 with password 'python310';
postgres=# create database agenda_curso with owner python310;
```

Despues de crear la base de datos, ejecuta los siguientes comandos en la consola dentro del directorio de nuestro proyecto con el entorno virtual activado, para realizar la migración de los modelos a la base de datos.

```
flask db upgrade
```

## Instalación de Gunicorn

Ejecuta el siguiente comando con el entorno virtual activado, para instalar gunicorn

```
pip install gunicorn
```

Ejecuta el siguiente comando con el entorno virtual activado, para levantar gunicorn indicando la instancia de tú aplicación

```
gunicorn --bind 0.0.0.0:5000 run:app
```

Abre tu navegador web y accede a la ruta [http://localhost:5000/swagger-ui](http://localhost:5000/swagger-ui)

## Creacion y configuracion de servicio

Crea un archivo de servicio.

```
sudo nano /etc/systemd/system/backend-agenda.service
```

Agrega el siguiente contenido al archivo **backend-agenda.service**

```
[Unit]
Description=Gunicorn instance to serve agenda
After=network.target

[Service]
User=debian
Group=www-data
WorkingDirectory=/home/debian/backend-app
Environment="PATH=/home/debian/backend-app/env/bin"
ExecStart=/home/debian/backend-app/env/bin/gunicorn --workers 3 --bind unix:backend-agenda.sock -m 007 run:app

[Install]
WantedBy=multi-user.target
```

Ejecuta los siguientes comandos para iniciar el servicio Gunicorn y habilitarlo para que se inicie en el arranque

```
sudo systemctl start backend-agenda.service
sudo systemctl enable backend-agenda.service
```
Ejecuta el siguiente comando para verificar que el servicio este iniciado
```
sudo systemctl status backend-agenda.service
```


## Instalación y configuración de Nginx

Ejecuta los siguintes comandos para instalar Nginx en **(Debian 11)**

```
sudo apt update
sudo apt install nginx
```

Crea un nuevo archivo de configuración de bloque de servidor

```
sudo nano /etc/nginx/sites-available/backend-agenda
```

Agrega el siguiente contenido al archivo **backend-agenda**

```
server {
    listen 5000;
    server_name agenda.posgrado.bo www.agenda.posgrado.bo;

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/debian/backend-agenda/backend-agenda.sock;
    }
}
```

Ejecuta el siguiente comando para vincular el archivo creado al directorio sites-enabled

```
sudo ln -s /etc/nginx/sites-available/backend-agenda /etc/nginx/sites-enabled/
```

Ejecuta el siguiente comando para probar errores de sintaxis en el archivo de configuración

```
sudo nginx -t
```

Ejecuta el siguiente comando para reiniciar el servidor Nginx

```
sudo systemctl restart nginx
```

Ejecuta el siguiente comando en en caso de error para ver errores

```
sudo nano /var/log/nginx/error.log
```

# Despliegue del proyecto usando Apache

Ejecuta el siguiente comando para instalar el servidor web

```
sudo apt update
sudo apt install apache2 libapache2-mod-wsgi-py3
```

Crea un archivo con extension **.wsgi** dentro del directorio de tú proyecto.

```
sudo nano /home/debian/backend-agenda/apache-flask.wsgi
```

Agrega el siguiente contenido al archivo **apache-flask.wsgi**

```
#! /usr/bin/python

activate_this = '/home/debian/backend-agenda/env/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))
import sys
sys.path.insert(0, '/home/debian/backend-agenda')
from run import create_app
application = create_app("config")
```

Crea un nuevo archivo de configuracion de virtual host

```
sudo nano /etc/apache2/sites-available/backend-agenda.conf
```

Agrega el siguiente contenido al archivo **backend-agenda.conf**

```
<VirtualHost *:80>
    WSGIDaemonProcess backend-agenda processes=4 threads=20
    WSGIScriptAlias / /home/debian/backend-agenda/apache-flask.wsgi
    SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
    <Directory "/home/debian/backend-agenda/">
        WSGIProcessGroup backend-agenda
        WSGIApplicationGroup %{GLOBAL}
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```
Ejecuta los siguientes comandos para vincular el archivo creado al directorio sites-enabled
```
sudo a2dissite 000-default.conf
sudo a2ensite backend-agenda.conf
```
Ejecuta el siguiente comando para probar errores de sintaxis en el archivo de configuración
```
sudo apache2ctl configtest
```
Ejecuta el siguiente comando para reiniciar el servidor Nginx
```
sudo service apache2 restart
```
Eejecuta el siguiente comando en en caso de error para ver errores

```
sudo tail -n 20 /var/log/apache2/error.log
```
