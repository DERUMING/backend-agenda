from app import db, ma
from marshmallow import Schema, fields, pre_load, validate

evento_usuario_table = db.Table(
    "evento_unidad", 
    db.metadata, 
    db.Column("evento_id", db.ForeignKey("evento.id", primary_key=True)),
    db.Column("usuario_id", db.ForeignKey("usuario.id", primary_key=True)),
    )


role_permission_table = db.Table(
    "role_permissions",
    db.metadata,
    db.Column("roles_id", db.ForeignKey("roles.id"), primary_key=True),
    db.Column("permissions_id", db.ForeignKey("permissions.id"), primary_key=True),
)

usuario_roles_table = db.Table(
    "user_roles",
    db.metadata,
    db.Column("usuario_id", db.ForeignKey("usuario.id"), primary_key=True),
    db.Column("roles_id", db.ForeignKey("roles.id"), primary_key=True),
)



class Usuario(db.Model):
    __tablename__ = 'usuario'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(256), nullable=False)
    ap_paterno = db.Column(db.String(256), nullable=True)
    ap_materno = db.Column(db.String(256), nullable=False)
    usuario = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.Text(), nullable=True)
    correo = db.Column(db.String(256), nullable=True)
    es_responsable = db.Column(db.Boolean, nullable=True)
    es_activo = db.Column(db.Boolean, nullable=True)
    fid_unidad = db.Column(db.Integer, db.ForeignKey('unidad.id', ondelete='CASCADE'), nullable=True)
    created_at = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updated_at = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    unidad = db.relationship('Unidad', backref=db.backref('usuarios', lazy='dynamic'))
    roles = db.relationship('Role', secondary=usuario_roles_table, backref=db.backref('usuarios', lazy='dynamic'))


    def __repr__(self):
        return "<Usuario(nombre='%s', ap_paterno='%s', ap_materno='%s', usuario='%s', password='%s', correo='%s', es_responsable='%s', es_activo='%s', fid_unidad='%s')>" % (
            self.nombre,
            self.ap_paterno,
            self.ap_materno,
            self.usuario,
            self.password,
            self.correo,
            self.es_responsable,
            self.es_activo,
            self.fid_unidad,
        )

class Unidad(db.Model):
    __tablename__ = 'unidad'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(256), nullable=False)
    es_activo = db.Column(db.Boolean, nullable=True)
    created_at = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updated_at = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    def __repr__(self):
        return "<Unidad(descripcion='%s', es_activo='%s')>" % (
            self.descripcion,
            self.es_activo,
        )


class Evento(db.Model):
    __tablename__ = 'evento'
    id = db.Column(db.Integer, primary_key = True)
    institucion = db.Column(db.Text(), nullable=False)
    tema = db.Column(db.Text(), nullable=False)
    lugar = db.Column(db.Text(), nullable=True)
    fecha = db.Column(db.Date, nullable=True)
    hora = db.Column(db.Time, nullable=True)
    estado = db.Column(db.String(256), nullable=True)
    es_activo = db.Column(db.Boolean, nullable=True)
    fid_created_by = db.Column(db.Integer, db.ForeignKey('usuario.id', ondelete='CASCADE'), nullable=True)
    fid_approved_by = db.Column(db.Integer, db.ForeignKey('usuario.id', ondelete='CASCADE'), nullable=True)
    created_at = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updated_at = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())
    
    usuarios = db.relationship('Usuario', secondary=evento_usuario_table, backref=db.backref('eventos', lazy='dynamic'))
    usuario_created = db.relationship('Usuario', foreign_keys=fid_created_by, backref=db.backref('eventos_created', lazy='dynamic'))
    usuario_approved = db.relationship('Usuario', foreign_keys=fid_approved_by, backref=db.backref('eventos_approved', lazy='dynamic'))



class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(256), nullable=False)
    createdAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updatedAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    def __repr__(self):
        return "<Role(nombre='%s')>" % (
            self.nombre,
        )

# modelo permissions


class Permission(db.Model):
    __tablename__ = 'permissions'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(256), nullable=False)
    createdAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updatedAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    def __repr__(self):
        return "<Permission(nombre='%s')>" % (
            self.nombre,
        )



""" 
--------------------------------------ESQUEMAS----------------------------------------
 """

class UsuarioSchema(ma.Schema):
    class Meta:
        ordered = True
    id = fields.Integer(dump_only=True)
    nombre = fields.String(required=True, error_messages={"required": "campo nombre es requerido."})
    ap_paterno = fields.String(required=False)
    ap_materno = fields.String(required=True, error_messages={"required": "campo ap_materno es requerido."})
    usuario = fields.String(required=True, error_messages={"required": "campo usuario es requerido."})
    password = fields.String(required=False, load_only=True)
    correo = fields.String(required=False, validate=validate.Email(error="No es un correo válido."))
    es_responsable = fields.Boolean(required=False)
    es_activo = fields.Boolean(required=False)
    fid_unidad = fields.Integer(required=False, allow_none=True)
    created_at = fields.DateTime(required=True, dump_only=True)
    updated_at = fields.DateTime(required=True, dump_only=True)

    unidad = fields.Nested(lambda: UnidadSchema(), dump_only=True)
    roles = fields.Nested(lambda: RoleSchema(), many=True, dump_only=True)


class UnidadSchema(ma.Schema):
    class Meta:
        ordered = True
    id = fields.Integer()
    descripcion = fields.String(required=True)
    es_activo = fields.Boolean(required=False)
    created_at = fields.DateTime(required=True)
    updated_at = fields.DateTime(required=True)


class EventoSchema(ma.Schema):
    class Meta:
        ordered = True
    id = fields.Integer()
    institucion = fields.String(required=True)
    tema = fields.String(required=True)
    lugar = fields.String(required=False)
    fecha = fields.Date(required=False)
    hora = fields.Time(required=False)
    estado = fields.String(required=False)
    es_activo = fields.Boolean(required=False)
    fid_created_by = fields.Integer(required=True)
    fid_approved_by = fields.Integer(required=True)
    created_at = fields.DateTime(required=True, dump_only=True)
    updated_at = fields.DateTime(required=True, dump_only=True)

    usuarios = fields.Nested(lambda: UsuarioSchema(), many=True, dump_only=True)
    usuario_created = fields.Nested(lambda: UsuarioSchema(), dump_only=True)
    usuario_approved = fields.Nested(lambda: UsuarioSchema(), dump_only=True)


class RoleSchema(ma.Schema):
    class Meta:
        ordered = True
    id = fields.Integer()
    nombre = fields.String(required=True)
    createdAt = fields.DateTime(required=True)
    updatedAt = fields.DateTime(required=True)

class PermissionSchema(ma.Schema):
    class Meta:
        ordered = True
    id = fields.Integer()
    nombre = fields.String(required=True)
    createdAt = fields.DateTime(required=True)
    updatedAt = fields.DateTime(required=True)


class LoginSchema(ma.Schema):
    class Meta:
        ordered = True
    usuario = fields.String(required=True, error_messages={"required": "Campo usuario es requerido."})
    password = fields.String(required=True, error_messages={"required": "Campo password es requerido."})


class UsuarioSwaggerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Usuario
        include_fk = True
        ordered = True
    
    id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    updated_at = fields.DateTime(dump_only=True)


class EventoSwaggerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Evento
        include_fk = True
        ordered = True
    
    id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    updated_at = fields.DateTime(dump_only=True)