from flask import Blueprint
from flask_restful import Api
from app.resources.Usuario import UsuarioListResource, UsuarioResource, LoginResource
from app.resources.Evento import EventoListResource, EventoResource

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

api.add_resource(UsuarioListResource, '/usuarios')
api.add_resource(UsuarioResource, '/usuarios/<int:id>')
api.add_resource(LoginResource, '/login')
api.add_resource(EventoListResource, '/eventos')
api.add_resource(EventoResource, '/eventos/<int:id>')