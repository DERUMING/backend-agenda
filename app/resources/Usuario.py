from flask_restful import Resource
from flask import request
from passlib.hash import pbkdf2_sha256 as sha256
from app.models import db, Usuario, UsuarioSchema, LoginSchema, UsuarioSwaggerSchema
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from app.functions import get_data
usuarios_schema = UsuarioSchema(many=True)
usuario_schema = UsuarioSchema()
usuario_update_schema = UsuarioSchema(partial=True)
usuario_swagger_schema = UsuarioSwaggerSchema(partial=True)


@doc(descripcion='Endpoint para manejo de usuarios', params={
    'Authorization': {
        'description': 'Authorization HTTP header con JWT token, Ej. Athorization: Bearer asda.asdfas.asdf',
        'in':'header',
        'type': 'string',
        'required': True
    }
}, tags=['Usuarios'])
class UsuarioListResource(MethodResource, Resource):

    @doc(params={
        'filter[nombre]': {
            'description': 'Nombre de la persona',
            'in': 'query',
            'type': 'string'
        },
        'filter[ap_paterno]': {
            'description': 'Apellido paterno de la persona',
            'in': 'query',
            'type': 'string'
        },
        'filter[ap_materno]': {
            'description': 'Apellido manterno de la persona',
            'in': 'query',
            'type': 'string'
        },
        'sort': {
            'description': 'Orden de registros según campo Ej. ASC ap_paterno Ej. DESC -ap_paterno',
            'in': 'query',
            'type': 'string'
        },
        'include': {
            'description': 'Relaciones de tabla que se van a incluir',
            'in': 'query',
            'type': 'string'
        },
        'page[size]': {
            'description': 'Número de registros por página',
            'in': 'query',
            'type': 'number'
        },
        'page[number]': {
            'description': 'Número de página',
            'in': 'query',
            'type': 'number'
        }
    })
    @jwt_required()
    def get(self):
        """ usuarios = Usuario.query.all()
        usuarios = usuarios_schema.dump(usuarios)
        return {"data": usuarios}, 200 """
        relations = ["unidad", "roles"]
        params = request.args.to_dict()
        result = get_data(Usuario,UsuarioSchema, relations,params, many=True)
        return result

    @use_kwargs(usuario_swagger_schema, location=('json'))
    @jwt_required()
    def post(self, **kwargs):
        json_data = request.get_json(force=True)
        error = usuario_schema.validate(json_data)
        if error:
            return { 'error': {'status': 422, 'title': 'Entidad no procesable', 'detail': error}}, 422

        usuario = Usuario.query.filter_by(usuario=json_data['usuario']).first()
        if usuario:
            return { 'error': {'status': 422, 'title': 'Entidad no procesable', 'detail': 'El usuario ya existe'}}, 422

        usuario = Usuario(
            nombre=json_data['nombre'],
            ap_paterno=json_data['ap_paterno'],
            ap_materno=json_data['ap_materno'],
            usuario=json_data['usuario'],
            password=sha256.hash(json_data['password']),
            correo=json_data['correo'],
            es_responsable=json_data['es_responsable'],
            es_activo=json_data['es_activo'],
            fid_unidad=json_data['fid_unidad']
        )

        db.session.add(usuario)
        db.session.commit()

        resultado = usuario_schema.dump(usuario)

        return {"data": resultado}, 201


@doc(descripcion='Endpoint para manejo de usuarios', params={
    'Authorization': {
        'description': 'Authorization HTTP header con JWT token, Ej. Athorization: Bearer asda.asdfas.asdf',
        'in':'header',
        'type': 'string',
        'required': True
    }
}, tags=['Usuarios'])
class UsuarioResource(MethodResource, Resource):
    @jwt_required()
    def get(self, id=None):
        usuario = Usuario.query.filter_by(id=id).first()
        if not usuario:
            return { 'error': {'status': 404, 'title': 'No encontrado', 'detail': 'El usuario solicitado no existe'}}, 404
        resultado = usuario_schema.dump(usuario)
        return { "data": resultado }, 200

    @use_kwargs(usuario_swagger_schema, location=('json'))
    @jwt_required()
    def put(self, id=None, **kwargs):
        usuario = Usuario.query.filter_by(id=id).first()
        if not usuario:
            return { 'error': {'status': 404, 'title': 'No encontrado', 'detail': 'El usuario solicitado no existe'}}, 404
        
        json_data = request.get_json(force=True)
        error = usuario_update_schema.validate(json_data)
        if error:
            return { 'error': {'status': 422, 'title': 'Entidad no procesable', 'detail': error}}, 422

        usuario.nombre = json_data.get('nombre', usuario.nombre)
        usuario.ap_paterno = json_data.get('ap_paterno', usuario.ap_paterno)
        usuario.ap_materno = json_data.get('ap_materno', usuario.ap_materno)
        usuario.usuario = json_data.get('usuario', usuario.usuario)
        usuario.password = usuario.password if json_data.get('password') is None else sha256.hash(json_data.get('password'))
        usuario.correo = json_data.get('correo', usuario.correo)
        usuario.es_responsable = json_data.get('es_responsable', usuario.es_responsable)
        usuario.es_activo = json_data.get('es_activo', usuario.es_activo)
        usuario.fid_unidad = json_data.get('fid_unidad', usuario.fid_unidad)

        db.session.commit()
        resultado = usuario_schema.dump(usuario)

        return { "data": resultado }, 200

    @jwt_required()
    def delete(self, id=None):
        #Usuario.query.filter_by(id=id).delete()
        usuario = Usuario.query.filter_by(id=id).first()
        if not usuario:
            return { 'error': {'status': 404, 'title': 'No encontrado', 'detail': 'El usuario solicitado no existe'}}, 404
        usuario.es_activo = False
        db.session.commit()
        return { "meta": {"mensaje": "usuario eliminado con éxito"}}, 200

@doc(description='Endpoint para autenticación de usuarios', tags=['Autenticación'])
class LoginResource(MethodResource, Resource):
    @use_kwargs(LoginSchema(), location=('json'))
    def post(self, **kwargs):
        json_data = request.get_json(force=True)
        login_schema = LoginSchema()
        error = login_schema.validate(json_data)
        if error:
            return { 'errors': {'status': 422, 'title': 'Entidad no procesable', 'detail': error} }, 422

        usuario = Usuario.query.filter_by(usuario = json_data.get('usuario')).first()

        if usuario and sha256.verify(json_data.get('password'), usuario.password):
            access_token = create_access_token(identity=usuario.nombre)
            refresh_token = create_refresh_token(identity=usuario.nombre)
            resultado = usuario_schema.dump(usuario)
            return { 'data': resultado, 'meta': { 'access_token': access_token, 'refresh_token': refresh_token } }, 200
        else:
            return { 'errors': { 'status': 401, 'title': 'No autorizado', 'detail': 'Credenciales incorrectas.' } }, 401
