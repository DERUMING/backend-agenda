from flask_restful import Resource
from flask import request
from passlib.hash import pbkdf2_sha256 as sha256
from app.models import db, Evento, EventoSchema, EventoSwaggerSchema
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
from flask_apispec import marshal_with, doc, use_kwargs
from flask_apispec.views import MethodResource
from app.functions import get_data
eventos_schema = EventoSchema(many=True)
evento_schema = EventoSchema()
evento_update_schema = EventoSchema(partial=True)
evento_swagger_schema = EventoSwaggerSchema(partial=True)


@doc(descripcion='Endpoint para manejo de eventos', params={
    'Authorization': {
        'description': 'Authorization HTTP header con JWT token, Ej. Athorization: Bearer asda.asdfas.asdf',
        'in':'header',
        'type': 'string',
        'required': True
    }
}, tags=['Eventos'])
class EventoListResource(MethodResource, Resource):

    @doc(params={
        'filter[institucion]': {
            'description': 'Nombre de la institución u organización que organiza el evento',
            'in': 'query',
            'type': 'string'
        },
        'filter[tema]': {
            'description': 'Temática del evento',
            'in': 'query',
            'type': 'string'
        },
        'filter[lugar]': {
            'description': 'Ubicación donde se realizará el evento',
            'in': 'query',
            'type': 'string'
        },
        'filter[fecha]': {
            'description': 'Fecha de la realización del evento Ej. 23/01/2022 o 2022-01-23',
            'in': 'query',
            'type': 'date'
        },
        'filter[estado]': {
            'description': 'Estado del evento',
            'in': 'query',
            'type': 'string'
        },
        'filter[es_activo]': {
            'description': 'Estado activo o inactivo del registro',
            'in': 'query',
            'type': 'boolean'
        },
        'sort': {
            'description': 'Orden de registros según campo Ej. ASC institucion Ej. DESC -institucion',
            'in': 'query',
            'type': 'string'
        },
        'include': {
            'description': 'Relaciones de tabla que se van a incluir',
            'in': 'query',
            'type': 'string'
        },
        'page[size]': {
            'description': 'Número de registros por página',
            'in': 'query',
            'type': 'number'
        },
        'page[number]': {
            'description': 'Número de página',
            'in': 'query',
            'type': 'number'
        }
    })
    @jwt_required()
    def get(self):
        relations = ["usuarios", "usuario_created", "usuario_approved"]
        params = request.args.to_dict()
        result = get_data(Evento, EventoSchema, relations, params, many=True)
        return result

    @use_kwargs(evento_swagger_schema, location=('json'))
    @jwt_required()
    def post(self, **kwargs):
        json_data = request.get_json(force=True)
        error = evento_schema.validate(json_data)
        if error:
            return { 'error': {'status': 422, 'title': 'Entidad no procesable', 'detail': error}}, 422

        evento = Evento(
            institucion=json_data['institucion'],
            tema=json_data['tema'],
            lugar=json_data['lugar'],
            fecha=json_data['fecha'],
            hora=json_data['hora'],
            estado=json_data['estado'],
            es_activo=json_data['es_activo'],
            fid_created_by=json_data['fid_created_by']
        )

        db.session.add(evento)
        db.session.commit()

        resultado = evento_schema.dump(evento)

        return {"data": resultado}, 201


@doc(descripcion='Endpoint para manejo de eventos', params={
    'Authorization': {
        'description': 'Authorization HTTP header con JWT token, Ej. Athorization: Bearer asda.asdfas.asdf',
        'in':'header',
        'type': 'string',
        'required': True
    }
}, tags=['Eventos'])
class EventoResource(MethodResource, Resource):
    @jwt_required()
    def get(self, id=None):
        evento = Evento.query.filter_by(id=id).first()
        if not evento:
            return { 'error': {'status': 404, 'title': 'No encontrado', 'detail': 'El evento solicitado no existe'}}, 404
        resultado = evento_schema.dump(evento)
        return { "data": resultado }, 200

    @use_kwargs(evento_swagger_schema, location=('json'))
    @jwt_required()
    def put(self, id=None, **kwargs):
        evento = Evento.query.filter_by(id=id).first()
        if not evento:
            return { 'error': {'status': 404, 'title': 'No encontrado', 'detail': 'El evento solicitado no existe'}}, 404
        
        json_data = request.get_json(force=True)
        error = evento_update_schema.validate(json_data)
        if error:
            return { 'error': {'status': 422, 'title': 'Entidad no procesable', 'detail': error}}, 422

        evento.institucion = json_data.get('institucion', evento.institucion)
        evento.tema = json_data.get('tema', evento.tema)
        evento.lugar = json_data.get('lugar', evento.lugar)
        evento.fecha = json_data.get('fecha', evento.fecha)
        evento.hora = json_data.get('hora', evento.hora)
        evento.estado = json_data.get('estado', evento.estado)
        evento.es_activo = json_data.get('es_activo', evento.es_activo)
        evento.fid_created_by = json_data.get('fid_created_by', evento.fid_created_by)

        db.session.commit()
        resultado = evento_schema.dump(evento)

        return { "data": resultado }, 200

    @jwt_required()
    def delete(self, id=None):
        #Evento.query.filter_by(id=id).delete()
        evento = Evento.query.filter_by(id=id).first()
        if not evento:
            return { 'error': {'status': 404, 'title': 'No encontrado', 'detail': 'El evento solicitado no existe'}}, 404
        evento.es_activo = False
        db.session.commit()
        return { "meta": {"mensaje": "evento eliminado con éxito"}}, 200

