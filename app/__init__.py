from flask import Flask
from flask_sqlalchemy  import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager
from flask_apispec import FlaskApiSpec
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
docs = FlaskApiSpec()

def create_app(config_filename='config'):
    app = Flask(__name__)

    app.config.from_object(config_filename)

    app.config.update({
        'APISPEC_SPEC': APISpec(
            title= 'API REST - AGENDA',
            version='V1.0 Cursos Posgrado',
            plugins=[MarshmallowPlugin()],
            openapi_version='2.0.0'
        ),
        'APISPEC_SWAGGER_URL': '/swagger/',
        'APISPEC_SWAGGER_UI_URL': '/swagger-ui/'
    })
    
    from app.models import Usuario, Unidad
    db.init_app(app)
    ma.init_app(app)    
    migrate.init_app(app, db)
    jwt = JWTManager(app)
    from app.routes.app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api/v1')


    from app.resources.Usuario import UsuarioListResource, UsuarioResource, LoginResource
    from app.resources.Evento import EventoListResource, EventoResource

    docs.init_app(app)
    docs.register(LoginResource, blueprint='api', endpoint='loginresource')
    docs.register(UsuarioListResource, blueprint='api', endpoint='usuariolistresource')
    docs.register(UsuarioResource, blueprint='api', endpoint='usuarioresource')
    docs.register(EventoListResource, blueprint='api', endpoint='eventolistresource')
    docs.register(EventoResource, blueprint='api', endpoint='eventoresource')

    return app