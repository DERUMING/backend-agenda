import re

def split(string, brackets_on_first_result=False):
    matches = re.split("[\[\]]+", string)
    matches.remove('')
    return matches


def mr_parse(params):
    results = {}
    for key in params.keys():
        if '[' in key:
            key_list = split(key)
            d = results
            for partial_key in key_list[:-1]:
                if partial_key not in d:
                    d[partial_key] = dict()
                d = d[partial_key]
            d[key_list[-1]] = params[key]
        else:
            results[key] = params[key]
    return results



def get_data(class_model, class_schema, exclude_fields, query_params, many=False):

    data = class_model.query
    q_params = mr_parse(query_params)
    page = {}
    if q_params:
        if "filter" in q_params:
            for attr, value in q_params['filter'].items():
                try:
                    if str(getattr(class_model, attr).property.columns[0].type) == 'BOOLEAN' or str(getattr(class_model, attr).property.columns[0].type) == 'DATE':
                        data = data.filter(getattr(class_model, attr)==value)
                    else:
                        data = data.filter(getattr(class_model, attr).ilike(f'%{value}%'))
                except AttributeError:
                    return {'error': {'status':400, 'title':'Petición incorrecta', 'detail': f'El Objeto {getattr(class_model, "__name__")} no tiene el atributo {attr}.'}}, 400

        if "sort" in q_params:
            campos = query_params['sort'].split(',')
            for field in campos:
                field = field.strip()
                try:
                    if field[0] == '-':
                        field = field.replace('-', '')
                        data = data.order_by(getattr(class_model, field).desc())
                    else:
                        data = data.order_by(getattr(class_model, field))
                except AttributeError:
                    return {'error': {'status':400, 'title':'Petición incorrecta', 'detail': f'El Objeto {getattr(class_model, "__name__")} no tiene el atributo {field}.'}}, 400

        if "page" in q_params:
            try:
                number = int(q_params['page']['number'])
                size = int(q_params['page']['size'])
            except ValueError:
                return {'error': {'status':400, 'title':'Petición incorrecta', 'detail': f'Page[number] y Page[size] deben ser números. '}}, 400
            except KeyError:
                return {'error': {'status':400, 'title':'Petición incorrecta', 'detail': f'Los parametros page[number] y page[size] deben ser enviados simultaneamente.'}}, 400

            data = data.paginate(page=number, per_page=size)
            page['current-page'] = data.page
            page['per-page'] = data.per_page
            page['from'] = data.first
            page['to'] = data.last
            page['total'] = data.total
            page['last-page'] = data.pages
        if "include" in q_params:
            relations = q_params['include'].split(',')
            for rel in relations:
                rel = rel.strip()
                if rel in exclude_fields:
                    exclude_fields.remove(rel)
                else:
                    return {'error': {'status':400, 'title':'Petición incorrecta', 'detail': f'El Objeto {getattr(class_model, "__name__")} no tiene relación con {rel}.'}}, 400
    else:
        data = data.all()

    data_schema = class_schema(many = many, exclude=exclude_fields)

    return {'data': data_schema.dump(data), 'meta': { 'page': page }}, 200


#q_params = { 'filter': { 'edad':'jose', 'ap_paterno':'mamani'}, 'sort': '-nombre,ap_paterno' 
# 'page':{'number': '1', 'size':'2'},
# 'include' : 'unidad'
# }

#http://127.0.0.1:5000/api/v1/usuarios?filter[es_activo]=false&sort=-nombre,ap_paterno
#http://127.0.0.1:5000/api/v1/usuarios?page[number]=1&page[size]=2
#http://127.0.0.1:5000/api/v1/usuarios?include=roles,unidad