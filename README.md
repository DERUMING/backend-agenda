# API REST AGENDA V1.0

Esta es la primera version de un Api Rest basado en Python y Flask, fue realizado para el curso de Desarrollo Backend en
la dirección de POSGRADO-UPEA.

## Comenzando 🚀   
_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

_Requerimientos mínimos del sistema._

1. Gestor de Base de datos PostgreSQL.
2. Python3.x.
3. virtualenv.

### Instalación 🔧

_Sigue los siguientes pasos despues de tener instalado todos los requisitos mensionados enteriormente._

Descarga el proyecto desde el repositorio, desde la consola ejecuta el siguiente comando dentro de un directorio de tu elección.
```
$ git clone https://gitlab.com/DERUMING/backend-agenda.git
```
Desde la consola de comando ingresa hasta el directorio del proyecto descargado ejecutando el siguiente comando.
```
$ cd /backend-agenda

```

Dentro del directorio del proyecto ejecuta los siguientes comandos para crear un entorno virtual y activarlo.

```
$ python -m virtualenv env
$ .\env\Scripts\activate
```
Despues de activar el entorno virtual, ejecuta el siguiente comando para instalar las librerias requeridas para el funcionamiento del sistema.

```
(env)$ pip install -r requirements.txt
```
A continuación crea un usuario y una base de datos ejecutando los siguientes comandos dentro de la consola __SQL Shell (psql)__ de PostgreSQL.
````
postgres=# create user python310 with password 'python310';
postgres=# create database agenda_curso with owner python310;
````
Despues de crear la base de datos, realiza la migración de los modelos a la base de datos ejecutando los siguientes comandos en la consola dentro del directorio de nuestro proyecto con el entorno virtual activado.
````
(env)$ flask db init
(env)$ flask db migrate -m "base de datos inicial"
(env)$ flask db upgrade
````
Puedes verificar la creación de las tablas ejecutando los siguientes comando dentro de la consola __SQL Shell (psql)__ de PostgreSQL.
````
postgres=# \c agenda_curso
agenda_curso=# \dt
````
Finalmente tienes listo el Api Rest para su funcionamiento y consumo, para poner en marcha el servidor de desarrollo ejecuta el siguiente comando.
````
(env)$ python run.py
````
## Ejecutando las pruebas ⚙️

Para observar el despliegue del sistema, ingresa el siguiente enlace http://localhost:5000/api/v1/usuarios en tu navegador, deberias obtener la lista de los registro de la tabla usuarios en formato JSON.
````
{
    "data": []
}
````
Para consumir los servicios del api rest te recomiendo que tengas instalado [Postman](https://www.postman.com/downloads/).


## Construido con 🛠️

* [Python](https://www.python.org/)
* [Flask](https://flask.palletsprojects.com/en/2.2.x/)
* [PostgreSQL](https://www.postgresql.org/)

## Autores ✒️

* **[Ing. Rolando Ulo Mollo](https://www.linkedin.com/in/rolando-ulo-mollo-069bab202)** - *Trabajo Inicial*

## Expresiones de Gratitud 🎁

* Agradecido con POSGRADO - UPEA.
